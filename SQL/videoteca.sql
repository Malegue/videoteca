DROP DATABASE IF EXISTS videoteca;
CREATE DATABASE videoteca;
USE videoteca;

DROP TABLE IF EXISTS regista;
CREATE TABLE regista (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  nome varchar(40) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS attore;
CREATE TABLE attore (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  nome varchar(40) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS categoria;
CREATE TABLE categoria (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  categoria varchar(20) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS film;
CREATE TABLE film (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  titolo varchar(50) NOT NULL,
  data_presentazione date DEFAULT NULL,
  trama varchar(500) NOT NULL,
  copertina varchar(255) DEFAULT NULL,
  regista int(10) unsigned NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (regista) REFERENCES regista (id) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS ca_fi;
CREATE TABLE ca_fi (
  categoria int(10) unsigned NOT NULL,
  film int(10) unsigned NOT NULL, 
  PRIMARY KEY (film,categoria),
  FOREIGN KEY (film) REFERENCES film (id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (categoria) REFERENCES categoria (id) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS at_fi;
CREATE TABLE at_fi (
  attore int(10) unsigned NOT NULL,
  film int(10) unsigned NOT NULL,
  PRIMARY KEY (attore,film),
  FOREIGN KEY (film) REFERENCES film (id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (attore) REFERENCES attore (id) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO regista (id, nome)
    VALUES (1,'Quentin Tarantino'),(2,'Tinto Brass'),(3,'Shane Black'),(5,'Sergio Corbucci');

INSERT INTO film (id, titolo, data_presentazione, trama, copertina, regista) 
    VALUES (1,'Chi lavora è perduto','1969-09-06','Bonifacio, giovane veneziano disoccupato e anarchico, fa una serie di strani incontri. Sullo sfondo di una Venezia inedita, è un film impregnato di veneta bizzarria libertaria che, tra scompensi e cadute di gusto, ha scatto, estro e qualche pagina di forza sconsolata, a mezza strada tra Rossellini e Godard. La censura impose tagli, modifiche e il cambio del titolo con In capo al mondo.','http://www.deastore.com/covers/803/270/621/batch3/8032706215282.jpg',2),
           (2,'Iron Man 3','2013-04-23','Sono passati piu'' di dieci anni da quando, nel corso di una serata in Svizzera, il miliardario genio-e-sregolatezza Tony Stark s''intratteneva con la bella Maya Hansen e lasciava sul tetto, in vana attesa, il visionario Aldrich Killian, desideroso di contare Stark tra le fila degli scienziati da lui diretti. Intanto, dal Medio Oriente, un terrorista senza scrupoli, detto il Mandarino, dopo aver seminato panico e morte in diverse zone degli Stati Uniti d''America, arriva.','http://pad.mymovies.it/filmclub/2012/06/004/locandina.jpg',3),
           (5,'Django Unchained','2013-01-17','Stati Uniti del Sud, alla vigilia della guerra civile. Il cacciatore di taglie di origine tedesca dottor King Schultz, su un carretto da dentista, è alla ricerca dei fratelli Brittle, per consegnarli alle autorità piuttosto morti che vivi e incassare la ricompensa. Per scovarli, libera dalle catene lo schiavo Django, promettendogli la liberta'' a missione completata. Tra i due uomini nasce così un sodalizio umano e professionale che li conduce attraverso l''America degli orrori.','http://pad.mymovies.it/filmclub/2011/05/035/locandina.jpg',1),
           (10,'Django', '1966-05-13', 'Django compare a piedi (occhi azzurri, passo pesante, cappellone sugli occhi) con la sella in spalla, trascinando una bara. Dopo 3 minuti ci sono 9 morti ammazzati. Allo scoccare della mezz''ora, siamo a quota 48. Il regista non si fa più passare per Sidney Corbett e, col nome vero, firma anche soggetto e sceneggiatura. L''inverosimiglianza della vicenda, le psicologie, i dialoghi, l''umor nero, sfiorano il delirio.', 'https://fbcdn-sphotos-c-a.akamaihd.net/hphotos-ak-ash3/74628_547727428572474_1137887748_n.jpg',5);


INSERT INTO attore (id, nome)
    VALUES  (1,'Robert Downey Jr.'),(2,'Gwinet Paltrow'),
            (3,'Don Cheadle'),(4,'Pascale Audret'),
            (5,'Tino Buazzelli'),(6,''),(8,'Uma Thurman'),
            (9,'Jamie Foxx'),(10,'Leonardo Di Caprio'),
            (11,'Samuel L. Jackson'),(12,'Loredana Nusciak'),
            (13,'Franco Nero');

INSERT INTO at_fi (attore, film)
    VALUES (3,1),(4,1),
           (1,2),(2,2),
           (9,5),(10,5),(11,5),
           (12,10),(13,10);

INSERT INTO categoria (id, categoria)
    VALUES (1,'Avventura'),(2,'Azione'),(3,'Thriller'),
           (4,'Horror'),(5,'Animazione'),(6,'Drammatico'),
           (7,'Commedia'),(8,'Romantico'),
           (9,'Fantascientifico'),(10,'Erotico'),
           (11,'Western');

INSERT INTO ca_fi (categoria, film)
    VALUES (2,2),(9,2),
           (2,5),
           (2,10),(11,10),
           (6,1),(10,1);