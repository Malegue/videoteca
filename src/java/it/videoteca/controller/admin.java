/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.videoteca.controller;
import it.videoteca.admin.CookieManage;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mattia
 */
public class admin extends HttpServlet {
    
    private String nomeAdmin = "root";
    private String pswAdmin = "toor";

    
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //Controllo l'operazione da eseguire
        String action = request.getParameter("action");
        action = (action == null) ? "" : action;
        //CREO L'ISTANZA DELLA CLASSE COOKIEMANAGE PER GESTIRE I COOKIE
        CookieManage cm = new CookieManage();
        //APRO LO STREAM OUTPUT
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        //BLOCCO AZIONE SET COOKIE
        if(action.equalsIgnoreCase("set")){
            
            String na = request.getParameter("na");
            String pa = request.getParameter("pa");
            
            if(na.equals(nomeAdmin) && pa.equals(pswAdmin)){
            
                Cookie setC = cm.setAdmin();
                response.addCookie(setC);
                out.println("<meta http-equiv=\"refresh\" content=\"0; url=manage.jsp\">");
            }
            
            else{
                out.println("Nome utente o password errati");
                out.println("<meta http-equiv=\"refresh\" content=\"1; url=login.jsp\">");
            }
            
        }
        //BLOCCO AZIONE LOGOUT
        else if(action.equalsIgnoreCase("logout")){
            
            Cookie setC = cm.outAdmin();
            response.addCookie(setC);
            out.println("<meta http-equiv=\"refresh\" content=\"0; url=login.jsp\">");
            
        }
        
        else{
            Cookie[] cookiesAdmin = request.getCookies();
            boolean check = cm.checkAdmin(cookiesAdmin);
            if(check) out.println("<meta http-equiv=\"refresh\" content=\"0; url=manage.jsp\">");
            else out.println("<meta http-equiv=\"refresh\" content=\"0; url=login.jsp\">");
        }
        
        
        out.close();
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
