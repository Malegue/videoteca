package it.videoteca.user;

import it.videoteca.admin.*;
import it.videoteca.dao.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class userop extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
               
        
        //Instanzio gli oggetti per la gestione del database
        DbManage dbmanage = new DbManage();
        DbQuery dbquery = new DbQuery();

        Connection connessioneDb;
        Statement statement;
        ResultSet risultatoQueryDb;
              
        
        
        try {
            
            connessioneDb = dbmanage.connettiDb();
            statement = connessioneDb.createStatement();

            //Recupero i dati dell'utente che ha effettuato il login
            String parola = request.getParameter("parola");
            System.out.print("Test recupero param: " + parola + "<br>");
            String queryDb = "SELECT * FROM film WHERE genere = '"+ parola +"'";
            //Eseguo l'eleliminazione
            risultatoQueryDb = dbquery.elaboraQuery(queryDb,statement);
                

            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();

            try {

                int contatoreFilm=0;  
                if(risultatoQueryDb.next()){
                  risultatoQueryDb.last();
                  contatoreFilm = risultatoQueryDb.getRow();
                }

                out.print("Numero tuple: " + contatoreFilm);

                risultatoQueryDb = dbquery.elaboraQuery(queryDb,statement);

                //Elaboro i risultati della query
                while (risultatoQueryDb.next()){
                    // ottiene il dato
                    String tempTitolo = risultatoQueryDb.getString("titolo");
                    String tempGenere = risultatoQueryDb.getString("genere");
                    String tempRegista = risultatoQueryDb.getString("regia");
                    String tempTrama = risultatoQueryDb.getString("trama");

                    out.print("<h2>" + tempTitolo + "</h2>");
                    out.print("Genere: " + tempGenere + "<br>");
                    out.print("Regia: " + tempRegista + "<br>");
                    out.print("Trama: " + tempTrama + "<br>");        
                    contatoreFilm ++;

                 }

                if(contatoreFilm == 0) out.print("<br><br>Nessun Risultato!");

    
            } finally {            
                out.close();
                //Chiudo la connessione con il database
                connessioneDb.close();

            }
       
        } 
        
        //Blocco complessivo del catch
        catch (ClassNotFoundException ex) {
            Logger.getLogger(process.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(process.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
