/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.videoteca.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mattia
 */
public class AdminDao {
    
    private DbManage db;
    private Connection connessioneDb;
    private DbQuery dbquery;
    
    public AdminDao(){
        
        this.db = new DbManage();
        this.connessioneDb = db.connettiDb();
        this.dbquery = new DbQuery();
    }
    
    public String filmSearch(String q){
        
        ResultSet risultatoQuery = null;
        Statement statementSearch;
        StringBuilder queryDB = new StringBuilder();
        StringBuilder tempS = new StringBuilder();
        int contatoreFilm = 0;
        
        try {
            
            statementSearch = connessioneDb.createStatement();
            //CONTROLLO SE LA STRINGA INSERITA NEL FORM DI RICERCA È VUOTA
            if(q.equals("")){
                            
                tempS.append("<div id=\"no-result\" class=\"alert alert-block half-width center-horizontally text-center modal-alert\">");
                tempS.append("<b class=\"label label-warning\">");
                tempS.append("ATTENZIONE!</b><br/>");
                tempS.append("<i class=\"icon-eye-close\"></i>");
                tempS.append(" Query non valida</div>");
             }
            
            else{
                //Creo la query
                String[] queryParam = q.split(" ");

                /* compongo la query */
                    
                    queryDB.append("SELECT film.id,film.titolo,");
                    queryDB.append("film.data_presentazione, regista.nome ");
                    queryDB.append("FROM film LEFT JOIN regista ON film.regista = regista.id WHERE ");
                    /* aggiungo le clausole WHERE dai parametri presi dall'utente */
                    for(int i = 0; i < queryParam.length; ++i) {
                        queryDB.append("film.titolo LIKE '%" + queryParam[i] + "%' OR ");
                        queryDB.append("regista.nome LIKE '%" + queryParam[i] + "%' ");
                        queryDB.append((i != queryParam.length -1) ? " OR " : ";");
                    }

                risultatoQuery = dbquery.elaboraQuery(queryDB.toString(),statementSearch);

                /* conto il numero di risultati ottenuti */

                    if(risultatoQuery.next()){
                      risultatoQuery.last();
                      contatoreFilm = risultatoQuery.getRow();
                    }
                    /* ricomincio dal primo risultato per stamparlo a schermo */
                    risultatoQuery.first();
                    
                    /* se la ricerca non ha prodotto risultati, avverto l'utente */
                    if(contatoreFilm == 0) {


                        tempS.append("<div id=\"no-result\" class=\"alert alert-block half-width center-horizontally text-center modal-alert\">");
                        tempS.append("<b class=\"label label-warning\">");
                        tempS.append("ATTENZIONE!</b><br/>");
                        tempS.append("<i class=\"icon-eye-close\"></i>");
                        tempS.append(" La ricerca &quot;"+q+" &quot; non ha prodotto alcun risultato</div>");
                    }

                    else{
                        
                        do{
                            
                            String filmID = risultatoQuery.getString("film.id");
                            String tempTitolo = risultatoQuery.getString("film.titolo");
                            String tempRegista = risultatoQuery.getString("regista.nome");
                            String tempData = risultatoQuery.getString("film.data_presentazione");
                            
                            tempS.append("<div id = '" + filmID +"' class=\"alert alert-info\">");
                            tempS.append("<a  class=\"close\" data-dismiss=\"alert\" href = 'javascript:deleteFilm(" + filmID + ");'>");
                            tempS.append("<img src = 'IMAGE/ICON/delete.png'></a>");
                            tempS.append("<h4>"+tempTitolo+"</h4>Regista: "+tempRegista+"<br/>Data: "+tempData);
                            tempS.append("</div>");
                           
                        } while (risultatoQuery.next());


                   }
            }
             
        
        
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tempS.toString();
    }
    
    
    
    
    public String deleteFilm(String id){
        
        int test;
        StringBuilder tempS = new StringBuilder();
        //query
        String queryDb1 = "DELETE FROM film WHERE film.id = "+ id +";";
        try{
            //Eseguo l'eleliminazione
            test = dbquery.insertQuery(queryDb1, connessioneDb);
            //Stampo la risposta
            if(test != 0){

                
                tempS.append("<div id=\"a-result\" class=\"alert alert-block half-width center-horizontally text-center modal-alert\">");
                tempS.append("<b class=\"label label-warning\">");
                tempS.append("SUCCESSO!</b><br/>");
                tempS.append("<i class=\"icon-eye-close\"></i>");
                tempS.append(" Eliminazione effettuata correttamente</div>");
                
            }
            else{
                
                tempS.append("<div id=\"a-result\" class=\"alert alert-block half-width center-horizontally text-center modal-alert\">");
                tempS.append("<b class=\"label label-warning\">");
                tempS.append("ATTENZIONE!</b><br/>");
                tempS.append("<i class=\"icon-eye-close\"></i>");
                tempS.append(" Errore eliminazione</div>");
                
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tempS.toString();
        
        
    }
    
    public String getRegista(){
        
            StringBuilder tempS = new StringBuilder();
            
        try {
            ResultSet risultatoQuery = null;
            Statement statementSearch;
            
            int contatoreFilm = 0;
            String queryDB = "SELECT regista.id, regista.nome FROM regista ORDER BY regista.nome";
            statementSearch = connessioneDb.createStatement();
            risultatoQuery = dbquery.elaboraQuery(queryDB.toString(),statementSearch);
            tempS.append("<select id = 'regista' required>");
            tempS.append("<option value=\"\"></option>");
            while (risultatoQuery.next()){
                            
                    String registaID = risultatoQuery.getString("regista.id");
                    String tempRegista = risultatoQuery.getString("regista.nome");
                    tempS.append("<option value = '"+registaID+"'>"+tempRegista+"</option>");                              
            }
            tempS.append("</select>");
            
            
            
            
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tempS.toString();
    }
    
    public String getAttore(int i){
        
            StringBuilder tempS = new StringBuilder();
            
        try {
            ResultSet risultatoQuery = null;
            Statement statementSearch;
            
            
            String queryDB = "SELECT attore.id, attore.nome FROM attore ORDER BY attore.nome";
            statementSearch = connessioneDb.createStatement();
            risultatoQuery = dbquery.elaboraQuery(queryDB.toString(),statementSearch);
            tempS.append("<select id = 'attore"+i+"'>");
            tempS.append("<option value = ''></option>");
            while (risultatoQuery.next()){
                          
                    String attoreID = risultatoQuery.getString("attore.id");
                    String tempAttore = risultatoQuery.getString("attore.nome");
                    tempS.append("<option value = '"+attoreID+"'>"+tempAttore+"</option>");
                    
            }
            tempS.append("</select>");
            
            
            
            
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tempS.toString();
    }
    
    
       public String getCategoria(int i){
        
            StringBuilder tempS = new StringBuilder();
            
        try {
            ResultSet risultatoQuery = null;
            Statement statementSearch;
            
            
            String queryDB = "SELECT * FROM categoria";
            statementSearch = connessioneDb.createStatement();
            risultatoQuery = dbquery.elaboraQuery(queryDB.toString(),statementSearch);
            tempS.append("<select id = 'categoria"+i+"'"+((i!=1)?"": "required")+">");
            tempS.append("<option value = ''></option>");
            while (risultatoQuery.next()){
                          
                    String categoriaID = risultatoQuery.getString("categoria.id");
                    String tempCategoria = risultatoQuery.getString("categoria.categoria");
                    tempS.append("<option value = '"+categoriaID+"'>"+tempCategoria+"</option>");
                    
            }
            tempS.append("</select>");
            
            
            
            
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return tempS.toString();
    }
       
       
    public String getIdFilm(String titolo){
        
            String filmID = null;
            
        try {
            ResultSet risultatoQuery = null;
            Statement statementSearch;
            String queryDB = "SELECT film.id FROM film WHERE film.titolo = '"+titolo+"'";
            statementSearch = connessioneDb.createStatement();
            risultatoQuery = dbquery.elaboraQuery(queryDB.toString(),statementSearch);
            
            while (risultatoQuery.next()){
                          
                    filmID = risultatoQuery.getString("film.id");
                    
            }
                               
            filmID = risultatoQuery.getString("film.id");
             
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return filmID;
    }
    
    
    
    
    public String insertAttore(String attore){
        try {
            String queryDB = "INSERT INTO attore (nome) values ('"+attore+"')";
            dbquery.insertQuery(queryDB, connessioneDb);
            
            
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "Attore inserito con successo";
    }
    
    public String insertRegista(String regista){
        try {
            String queryDB = "INSERT INTO regista (nome) values ('"+regista+"')";
            dbquery.insertQuery(queryDB, connessioneDb);
            
            
        } 
        catch (ClassNotFoundException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        } 
        catch (SQLException ex) {
            Logger.getLogger(AdminDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "Regista inserito con successo <img src = 'IMAGE/ICON/load.gif'>";
    }
    
    
   
    
}
