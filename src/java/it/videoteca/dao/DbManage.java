package it.videoteca.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbManage {
    
    private String password;
    private String utenteDb;
    private String nomeDb;
    private Connection connessioneDb;
    
    public DbManage(){
        
        this.utenteDb = "root";
        this.password = "3186";
        this.nomeDb = "videoteca";
        this.connessioneDb = null;
    }
    
    public Connection connettiDb(){
        
        
        try {
            
            Class.forName("com.mysql.jdbc.Driver");
            // apre la connessione con il database
            connessioneDb = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+ nomeDb, utenteDb, password);
        } 
        catch (SQLException ex) {
            Logger.getLogger(DbManage.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ClassNotFoundException ex) {
                Logger.getLogger(DbManage.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return connessioneDb;
    }
    
    public void disconettiDb() {
        
        try {
            connessioneDb.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbManage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
