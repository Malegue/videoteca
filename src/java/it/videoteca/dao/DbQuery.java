package it.videoteca.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import java.sql.SQLException;

public class DbQuery {
    
    public DbQuery(){
        this.risultatoQueryDb = null;        
    }
    
    public ResultSet elaboraQuery(String query,Statement statementDb) throws ClassNotFoundException, SQLException{
        
        risultatoQueryDb = statementDb.executeQuery(query);
        return risultatoQueryDb;        
    }
    
    public int insertQuery(String query, Connection connectionDb) throws ClassNotFoundException, SQLException{
        PreparedStatement pst = connectionDb.prepareStatement(query);
        int numRowsChanged = pst.executeUpdate();
        return numRowsChanged;
    }
    
    private ResultSet risultatoQueryDb;
}
