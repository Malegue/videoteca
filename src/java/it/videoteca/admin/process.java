package it.videoteca.admin;

import it.videoteca.dao.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "process", urlPatterns = {"/process"})
public class process extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        CookieManage cm = new CookieManage();
        Cookie[] cookiesAdmin = request.getCookies();
        boolean check = cm.checkAdmin(cookiesAdmin);
        
        if(!check){
            
           out.println("<meta http-equiv=\"refresh\" content=\"0; url=login.jsp\">");
            
        }
        else{
        
        //recupero i parametri
        String action = request.getParameter("action");
        action = (action == null) ? "" : action;
        
        //Instanzio gli oggetti per la gestione del database
        DbManage dbmanage = new DbManage();
        DbQuery dbquery = new DbQuery();
        AdminDao adminDao = new AdminDao();

        //Creo gli oggetti necessari per la connessione
        Connection connessioneDb = null;
        
        //Creo i contatori e variabili necessarie
        int contatoreFilm=0;
        int test = 0;
        
        //Creo la stringa per il dsearch
        String answerAction = null;
        
       
        
        
        
        try {
            
                //Connessione al database
                connessioneDb = dbmanage.connettiDb();

                
                //Blocco codice insert attore
                if(action.equalsIgnoreCase("regista")){
                    String nr = request.getParameter("nr");
                    nr = (nr == null) ? "" : nr;
                    if(nr.equals("")){
                        out.print("Errore nell'inserimento del regista!");
                    }
                    else{
                        out.print(adminDao.insertRegista(nr));
                    }
                    
                    
                }
                
                //Blocco codice insert attore
                if(action.equalsIgnoreCase("attore")){
                    String na = request.getParameter("na");
                    na = (na == null) ? "" : na;
                    if(na.equals("")){
                        out.print("Errore nell'inserimento dell'attore!");
                    }
                    else{
                        out.print(adminDao.insertAttore(na));
                    }
                    
                    
                }
                //Blocco codice insert
                if(action.equalsIgnoreCase("insert")){
                   
                    
                    //Recupero i parametri inviati
                    String titolo = request.getParameter("titolo");
                    String data = request.getParameter("data");
                    String regista = request.getParameter("regista");
                    String categoria1 = request.getParameter("categoria1");
                    String categoria2 = request.getParameter("categoria2");
                    String attore1 = request.getParameter("attore1");
                    String attore2 = request.getParameter("attore2");
                    String attore3 = request.getParameter("attore3");
                    String copertina = request.getParameter("copertina");
                    String trama = request.getParameter("trama").replaceAll("'", "''");
                    
                    
                    
                         
                    
                    
                    if(titolo.isEmpty() || data.isEmpty() || regista.isEmpty() || categoria1.isEmpty()  ||  trama.isEmpty()){
                        
                        test = 0;
                       
                    }
                    
                    else{
                        
//                    out.println(titolo);
//                    out.println(data);
//                    out.println(regista);
//                    out.println(categoria1);
//                    out.println(categoria2);
//                    out.println(attore1);
//                    out.println(attore2);
//                    out.println(attore3);
//                    out.println(copertina);
//                    out.println(trama);
                              
                        String[] tempData = data.split("/");
                        data = tempData[2] + "-" + tempData[0] + "-" + tempData[1];
                        String queryFilmInsert = "INSERT INTO film (titolo,data_presentazione,trama,copertina,regista) values ('"+titolo+"','"+data+"','"+trama+"', '"+copertina+"', '"+regista+"')";
                        test = test + dbquery.insertQuery(queryFilmInsert, connessioneDb);
                        String filmId = adminDao.getIdFilm(titolo);
//                        out.println(test);
//                        out.println(filmId);
                        
                        String queryCaFiInsert = "INSERT INTO ca_fi (film,categoria) values ('"+filmId+"','"+categoria1+"')";
                        test = test + dbquery.insertQuery(queryCaFiInsert, connessioneDb);
                        
                        if(!categoria2.isEmpty()){
                        String queryCaFiInsert2 = "INSERT INTO ca_fi (film,categoria) values ('"+filmId+"','"+categoria2+"')";
                        test = test + dbquery.insertQuery(queryCaFiInsert2, connessioneDb);
                        }
                        
                        String queryAtFiInsert1;
                        String queryAtFiInsert2;
                        String queryAtFiInsert3;
                        
                        if(!attore1.isEmpty()){
                            queryAtFiInsert1 = "INSERT INTO at_fi (attore,film) values ('"+attore1+"','"+filmId+"')";
                            test = test + dbquery.insertQuery(queryAtFiInsert1, connessioneDb);
                        }
                        
                        if(!attore2.isEmpty()){
                            queryAtFiInsert2 = "INSERT INTO at_fi (attore,film) values ('"+attore2+"','"+filmId+"')";
                            test = test + dbquery.insertQuery(queryAtFiInsert2, connessioneDb);
                        }
                        
                        if(!attore3.isEmpty()){
                            queryAtFiInsert3 = "INSERT INTO at_fi (attore,film) values ('"+attore3+"','"+filmId+"')";
                            test = test + dbquery.insertQuery(queryAtFiInsert3, connessioneDb);
                        }
                        
                        
                    
                    
                    
                    //out.print(test);
                    }
                    if(test != 0){
                        
                        out.print("<br><div class=\"alert alert-success\">");
                        out.print("<b>Congratulazioni!</b> Film inserito con successo!");
                        out.print("</div>");
                        
                        
                        
                    }
                    else{
                        
                        out.print("<br><div class=\"alert alert-error\">");
                        out.print("<b>Ops!</b> Problemi con l'inserimento del film");
                        out.print("</div>");
                    }
                    
                    
                                                            
                }

                //BLOCCO CODICE DELETE
                if(action.equalsIgnoreCase("delete")){

                    String id = request.getParameter("id");
                    answerAction = adminDao.deleteFilm(id);
                    out.print(answerAction);
                    

                }
                
                //BLOCCO CODICE SEARCH 4 DELETE
                if(action.equalsIgnoreCase("dsearch")){
                    
                    //Recupero i parametri
                    String temp = request.getParameter("q");
                    temp = (temp == null) ? "" : temp;
                    
                    /* elimina eventuali problemi di sicurezza legati alla possibilità di fare SQL injection */
                    temp = temp.replaceAll("[^A-Za-z0-9\\s]","");
                    
                    answerAction = adminDao.filmSearch(temp);                   
                    //Invio la risposta
                    out.print(answerAction);

                 }
                
                connessioneDb.close();
        }
              
        //Blocco complessivo del catch
        catch (ClassNotFoundException ex) {
            //Logger.getLogger(process.class.getName()).log(Level.SEVERE, null, ex);
            out.print(ex.getMessage());
            
        } catch (SQLException ex) {
            //Logger.getLogger(process.class.getName()).log(Level.SEVERE, null, ex);
            out.print(ex.getMessage());
        }

    }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
