/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.videoteca.admin;

import javax.servlet.http.Cookie;

/**
 *
 * @author mattia
 */
public class CookieManage {
    
    private String nomeC;
    private String valoreC;
    
    public CookieManage(){
        this.nomeC = "videoteca";
        this.valoreC = "adminOk";
    }
    
    public Boolean checkAdmin(Cookie[] cookiesAdmin){
        
        boolean check = false;        
        // indice per la gestione del ciclo
        int indice = 0;

        while (indice < cookiesAdmin.length) {
            // esegue il ciclo fino a quando ci sono elementi in cookieUtente
           
            if (cookiesAdmin[indice].getName().equals("videoteca")){
                break;
            }
            
            indice ++; 
        }// while 
        
        if (indice < cookiesAdmin.length){
            // il cookie è stato trovato e viene messo nell'oggetto mioCookie
            check = true;   
        }
        
        else    check = false;
        
        return check;
    }
    
    
    
    public Cookie setAdmin(){
        
                // definisce un oggetto Cookie con e la coppia 
        // nome-valore da memorizzare
        Cookie mioCookie = new Cookie ("videoteca", "adminSet");

        // specifica il percorso del cookie
        // che ha il privilegio di scrittura e lettura 
        // se omesso è inteso il percorso corrente
        mioCookie.setPath("/");

        
        // scrive il cookie
       return mioCookie;
    }
    
    public Cookie outAdmin(){
        
        Cookie nomecookie = new Cookie("videoteca", null); 
        nomecookie.setMaxAge(0); 
        nomecookie.setPath("/"); 
        return nomecookie;
    }
    
}
