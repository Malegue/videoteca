package it.videoteca.fe;

public abstract class Header {
    
    public static String setActive(String filename, String link) {
       
        if(!filename.equals(link)) {
            return "navbar-focus";
        }
        return "active";
    }
}
