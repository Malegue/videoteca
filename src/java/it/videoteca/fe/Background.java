package it.videoteca.fe;

public abstract class Background {
    
    public static String getRandomLink() {
        int random = (int)(Math.random() * 20 +1);
        return (random < 10 ? "0" : "") + random;
    }
}
