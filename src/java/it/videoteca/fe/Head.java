package it.videoteca.fe;

public abstract class Head {
    public static String getTitle(String filename) {
        
        String title;
        if (filename.equals("about_jsp")) title = "Su di noi";
        else if (filename.equals("films_jsp")) title = "Lista Film";
        else if (filename.equals("search_jsp")) title = "Ricerca";
        else title = "Videoteca";
        
        return title;
    }
}
