<%@page import="it.videoteca.admin.CookieManage"%>
<%@page import="it.videoteca.dao.AdminDao"%>
<%@page import="it.videoteca.fe.Background"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    
    Cookie[] cookiesAdmin = request.getCookies();
    CookieManage cm = new CookieManage();
    boolean check = cm.checkAdmin(cookiesAdmin);
    if(!check) out.print("<meta http-equiv=\"refresh\" content=\"0; url=login.jsp\">");
    else{
        
        AdminDao adminOp = new AdminDao();
        

%>

<!DOCTYPE html>
<html>
    <!-- HEAD DELLA PAGINA CON LE INCLUSIONI DEI CSS E I META TAG PER LA DESCRIZIONE DEL SITO -->
    <%@include file="head.jsp" %>
    
    <!-- CORPO DELLA PAGINA -->
    <body>
        <!-- NAVBAR A INIZIO PAGINA -->
        <%@include file="headeradmin.jsp" %>

<%
    //Recupero l'azione da eseguire
    String action = null;
    action = request.getParameter("action");
    action = (action == null) ? "" : action;

    //inserimento
    if(action.equals("insert")){

        %>
       <script>
        $(function() {
          $("#data").datepicker();
        });
        </script>

        <div class="container">
            <br>
            <table class="table table-hover width-100 center-horizontally">
                <thead>
                <tr>
                  <th class="span2 label label-info">Inserisci Film</th>
                  
                </tr>
              </thead>
            </table>
            <br>
            <input type="hidden" id = "action" name ="action" value="insert"/>
            <form id = "insert" accept-charset="UTF-8">
                
            <table class="table table-hover width-100 center-horizontally">
                <tr>
                    <td>Titolo: </td>
                    <td>
                        <input type="text" id = "titolo" name="titolo" required/>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Data Presentazione:</td>
                    <td>
                        <input type="text"  id = "data" name="data" placeholder="mm/dd/yyyy" required/>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Regista:</td>
                    <td><% out.print(adminOp.getRegista()); %></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Aggiungi regista (se non è presente nella lista):</td>
                    <td>
                        <input type="text"  id = "registaAdd" name="registaAdd"/>
                        <input type="button" class="btn" id = "addRegista" value="aggiungi"/>
                        <div id="registaAddResponse"></div>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>categoria:</td>
                    <td><% out.print(adminOp.getCategoria(1)); %></td>
                    <td><% out.print(adminOp.getCategoria(2)); %></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Attori:</td>
                    <td><% out.print(adminOp.getAttore(1)); %></td>
                    <td><% out.print(adminOp.getAttore(2)); %></td>
                    <td><% out.print(adminOp.getAttore(3)); %></td>
                </tr>
                <tr>
                    <td>Aggiungi attore (se non è presente nella lista):</td>
                    <td>
                        <input type="text"  id = "attore" name="attore"/>
                        <input type="button" class="btn" id = "addActor" value="aggiungi"/>
                        <div id="actorAddResponse"></div>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Copertina: (Inserisci url immagine)</td>
                    <td>
                        <input type="text"  id = "copertina" name="copertina" value=""/>
                        <br/>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Trama:</td>
                    <td>
                        <textarea required class="text-area" rows="10" cols="100"  id = "trama" maxlength="500" name="trama"></textarea>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <input type="button" class="btn" id = "submitInsert" value="invia"/>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
             </table>
        </form>
        <div id="insertResponse"></div>
        <br/>
        <br/>
        </div>

        <%
    }

    else if(action.equals("delete")) {

        %>
        
        <div class="container ">
            <br>
            <table class="table table-hover width-100 center-horizontally">
                <thead>
                <tr>
                  <th class="span2 label label-info">Elimina Film</th>
                </tr>
              </thead>
            </table>
            <br>
            <form id = "searchDelete">
                Cerca il film che vuoi eliminare:<br>
                <input type="text" id = "q" name="q" placeholder="film" autofocus>
                <input type="button"  class="btn" id = "submitDelete" value="Cerca">
            </form>
            <div id="deleteResponse"></div>
            <div id="deleteSearch"></div>
        </div>
        <div class="container" style="padding-top: 24%;"></div>
        
      <% 
     }
    else {
        %>
        
        <div class="body-container container"
             style="background-image: url('IMAGE/BACKGROUND/01.jpg');">
            <!-- LOGO -->
            <div class="text-center">
                <span class="logo">Gestione</span>
            </div>
        </div>
<%
    }
%>
    <div>
        
            <%@include file="footer.jsp" %>
    </div>
    </body>
</html>
<%
    }
%>
