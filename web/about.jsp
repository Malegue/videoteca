<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <!-- HEAD DELLA PAGINA CON LE INCLUSIONI DEI CSS E I META TAG PER LA DESCRIZIONE DEL SITO -->
    <%@include file="head.jsp" %>
    
    <!-- CORPO DELLA PAGINA -->
    <body>
        <!-- NAVBAR A INIZIO PAGINA -->
        <%@include file="header.jsp" %>
        
        <div id="body-container" style="background-image: url('IMAGE/BACKGROUND/01.jpg');">
            <div class="container">
                <br/>
                <br/>
                <div class="hero-unit text-center">
                    <h1 class="shadow-cool">SU DI NOI</h1>
                    <p>Qualche notizia sui creatori di questo sito web!</p>
                    <br/>
                    <div class="media well width-80 center-horizontally">
                        <a class="pull-left">
                            <img class="media-object little-photo" src="IMAGE/NOI/francesco-marano.jpg">
                        </a>
                        <div class="media-body">
                          <h4 class="media-heading text-info">Francesco Marano</h4>
                          <p>Un po' di testo tanto per raccontarci un po' con stile</p>
                        </div>
                    </div>
                    <br/>
                    <div class="media well width-80 center-horizontally">
                        <a class="pull-right">
                            <img class="media-object little-photo" src="IMAGE/NOI/mattia-castiglione.jpg">
                        </a>
                        <div class="media-body">
                          <h4 class="media-heading text-info">Mattia Castiglione</h4>
                          <p>Un po' di testo tanto per raccontarci un po' con stile.</p>
                        </div>
                    </div>
                    <br/>
                    <div class="media well width-80 center-horizontally">
                        <a class="pull-left">
                            <img class="media-object little-photo" src="IMAGE/NOI/emanuele-ristoratore.jpg">
                        </a>
                        <div class="media-body">
                          <h4 class="media-heading text-info">Emanuele Ristoratore</h4>
                          Un po' di testo tanto per raccontarci un po' con stile
                        </div>
                    </div>
                    <br/>
                    <p>
                      <a class="btn btn-info btn-large" href="https://bitbucket.org/mrnfrancesco/videoteca/wiki/Home" target="_blank">
                        Wiki
                      </a>
                      <a class="btn btn-warning btn-large" href="https://bitbucket.org/mrnfrancesco/videoteca/issues/" target="_blank">
                        Issue
                      </a>
                      <a class="btn btn-success btn-large" href="https://bitbucket.org/mrnfrancesco/videoteca/wiki/CHANGELOG" target="_blank">
                        Changelog
                      </a>
                    </p>
                </div>
            </div>
        </div>
             
        <!-- FOOTER PER I CREDITS FISSA A FONDO PAGINA -->
        <%@include file="footer.jsp" %>
    </body>
</html>