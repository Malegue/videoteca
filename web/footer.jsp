<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- FOOTER CON TUTTI I CREDITS -->
<footer class="footer">
    <div class="container">
        <p>Designed and built by 
            <a href="./about.jsp" target="_blank">Francesco Marano</a>
            ,
            <a href="./about.jsp" target="_blank">Mattia Castiglione</a>
            and
            <a href="./about.jsp" target="_blank">Emanuele Ristoratore</a>
        </p>

        <p>Code licensed under <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a>, documentation under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
        <p><a href="http://glyphicons.com">Glyphicons Free</a> licensed under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
        <ul class="footer-links">
          <li><a href="https://bitbucket.org/mrnfrancesco/videoteca/wiki/Home" target="_blank">Wiki</a></li>
          <li class="muted">·</li>
          <li><a href="https://bitbucket.org/mrnfrancesco/videoteca/issues/" target="_blank">Issues</a></li>
          <li class="muted">·</li>
          <li><a href="https://bitbucket.org/mrnfrancesco/videoteca/wiki/CHANGELOG" target="_blank">Changelog</a></li>
        </ul>
    </div>
</footer>

<!-- AVVIO TUTTI I JS ALLA FINE PER NON RALLENTARE IL CARICAMENTO -->
<script src="js/jquery.min.js"></script>
<script src="js/ajaxmanage.js"></script>
<script src="js/ajaxuser.js"></script>
<script src="js/bootstrap-modal.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery-ui.js"></script>