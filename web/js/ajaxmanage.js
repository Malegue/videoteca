//Script ajax per inserimento film
                       
$('#submitInsert').click(function(event) {  

    var titolo=$('#titolo').val();
    var data=$('#data').val();
    var trama=$('#trama').val();
    var copertina=$('#copertina').val();
    var regista=$('#regista').val();
    var attore1=$('#attore1').val();
    var attore2=$('#attore2').val();
    var attore3=$('#attore3').val();
    var categoria1=$('#categoria1').val();
    var categoria2=$('#categoria2').val();

$.get('process',{action:'insert',titolo:titolo,data:data,trama:trama,copertina:copertina,regista:regista,attore1:attore1,attore2:attore2,attore3:attore3,categoria1:categoria1,categoria2:categoria2},function(responseText) { 
        $('#insertResponse').html(responseText);
        if(!responseText.contains("error")){
            setTimeout(function(){
              document.location.reload(true);
            },3000);                       
        }
     });
});

//Script ajax per inserimento film
$('#addActor').click(function(event) {  
    var attore=$('#attore').val();

$.get('process',{action:'attore',na:attore},function(responseText) { 

    $('#actorAddResponse').html(responseText);
     setTimeout(function(){
        document.location.reload();
      },2000);
    });
});


//Script ajax per inserimento film
$('#addRegista').click(function(event) {  
    var regista=$('#registaAdd').val();

$.get('process',{action:'regista',nr:regista},function(responseText) { 
    $('#registaAddResponse').html(responseText);

     setTimeout(function(){
        document.location.reload();
      },2000);
    });
});


//Script ajax per inserimento film
$(document).ready(function() {                        
    $('#addImgButton').click(function(event) {  
        var img=$('#addImg').val();
        
    $.post('UploadImg', function(responseText) { 
       
        $('#ImgAddResponse').html(responseText);
        setTimeout(function(){
            document.location.reload();
          },2000);
       
        });
    });
});

//Script ajax per eliminazione film
$(document).ready(function() {
    
    $('#submitDelete').click(cerca);
    $('#searchDelete').submit(cerca);
            
    function cerca(){
            var q=$('#q').val();

            $.get('process',{q:q,action:'dsearch'},function(responseText) {
            
            $('#deleteSearch').html(responseText);
            
            $('#deleteResponse').html("");
            
            var e = document.getElementById('no-result');
            if (e.style.visibility === 'hidden') {
                e.style.visibility = 'visible';
                e.style.display = 'block';
                
            }
            setTimeout(function() {
                    $('#no-result').fadeOut('slow');
                }, 2000);
                setTimeout(function() {
                    e.style.visibility = 'hidden';
                    e.style.display = 'none';
                },3000);
        });
        return false;
    }
});



// Funzione che mi permette di ricevere tramite ajax il messaggio di rispota dalla funzione delete della servlets
function deleteFilm(id){
    
    $.get('process',{id:id,action:'delete'},function(responseText) { 
            
            $('#deleteResponse').html(responseText);
            $('#'+id).fadeOut('slow');
        });
        
        var g = document.getElementById('a-result');
        if (g.style.visibility === 'hidden') {
            g.style.visibility = 'visible';
            g.style.display = 'block';
            setTimeout(function() {
                $('#a-result').fadeOut('slow');
            }, 2000);
            setTimeout(function() {
                g.style.visibility = 'hidden';
                g.style.display = 'none';
            },3000);
        }
}