<%@page import="it.videoteca.fe.Head"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

    <!-- INTESTAZIONE DEL SITO CON TUTTE LE SPECIFICA TRAMITE META TAG E LE IMPORTAZIONI DEI CSS -->
    <head>
        <meta http-equiv="Content-Type" content ="text/html; charset=UTF-8">
        
        <link rel="stylesheet" href="CSS/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="CSS/docs.css" type="text/css"/>
        <link rel="stylesheet" href="CSS/background.css" type="text/css"/>
        <link rel="stylesheet" href="CSS/jquery-ui.css" type="text/css"/>
        
        <!-- Cambia l'icona del sito -->
        <link rel="shortcut icon" href="favicon.ico"/>
        
        <!-- TITOLO DELLA PAGINA SULLA SCHEDA DEL BROWSER -->
        <title>
            <% out.print(Head.getTitle(getClass().getSimpleName())); %>
        </title>
    </head>
    