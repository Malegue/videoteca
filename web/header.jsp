<%@page import="it.videoteca.fe.Header"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- NAVBAR A INIZIO DI TUTTE LE PAGINE -->
<header>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <!-- TITOLO A INIZIO DELLA NAVBAR -->
            <a class="brand">Videoteca</a>
            <!-- IL RESTO DELLA NAVBAR DOPO IL TITOLO -->
            <div class="nav-collapse collapse">
                <!-- MENU' A SINISTRA CON I LINK ALLE SEZIONI DEL SITO -->
                <ul class="nav">
                    <%
                        String filename = getClass().getSimpleName().replaceAll("_", ".");
                    %>
                    <li class="<% out.print(Header.setActive(filename, "index.jsp")); %>">
                        <a href="index.jsp"><i class="icon-home"></i>  Home</a>
                    </li>
                    <li class="<% out.print(Header.setActive(filename, "search.jsp")); %>">
                        <a href="search.jsp"><i class="icon-film"></i>  Lista film</a>
                    </li>
                </ul>
                <!-- MENU' A DESTRA CON IL LINK ALLA SEZIONE ABOUT DEL SITO -->
                <ul class="nav pull-right">
                    <li class="<% out.print(Header.setActive(filename, "about.jsp")); %>">
                        <a href="about.jsp"><i class="icon-user"></i>  Su di noi</a>
                    </li>
                    <li class="<% out.print(Header.setActive(filename, "manage.jsp")); %>">
                        <a href="manage.jsp"><i class="icon-user"></i>Admin</a>
                    </li>
                </ul>
                <!-- BARRA DI RICERCA SOLO SE STIAMO NELLA PAGINA search.jsp --> 
                <%
                    if (filename.equals("search.jsp")) {
                %>
                        <form method="post" action="search.jsp" class="navbar-search pull-right">
                            <input name="q" type="text" class="search-query" placeholder="Cerca..." autofocus>
                        </form>
                <%
                    }
                %>
            </div>
        </div>
      </div>
    </div>
</header>