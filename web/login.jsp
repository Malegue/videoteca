<%@page import="it.videoteca.admin.CookieManage"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Cookie[] cookiesAdmin = request.getCookies();
    CookieManage cm = new CookieManage();
    boolean check = cm.checkAdmin(cookiesAdmin);
    if(check) out.println("<meta http-equiv=\"refresh\" content=\"4; url=manage.jsp\">");
    else {
%>

<!DOCTYPE html>
<html lang="it">
  <head>
    <meta http-equiv="Content-Type" content ="text/html; charset=UTF-8">
    <title>Log in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Cambia l'icona del sito -->
    <link rel="shortcut icon" href="favicon.ico"/>
    
    <link href="CSS/bootstrap.css" rel="stylesheet">
    <link href="CSS/bootstrap-responsive.css" rel="stylesheet">
    
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
    </style>
  </head>
  <body>
    <div class="container">
        <form class="form-signin" action ="admin">
          <h2 class="form-signin-heading">Videoteca</h2>
          <input type="hidden" class="input-block-level" name ="action" value="set">
          <input type="text" class="input-block-level" placeholder="Nome Utente" name ="na">
          <input type="password" class="input-block-level" placeholder="Password" name ="pa">
          <button class="btn btn-large btn-primary" type="submit">Accedi</button>
        </form>
    </div> 
  </body>
</html>
<%
    }
%>