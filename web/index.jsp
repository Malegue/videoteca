<%@page import="it.videoteca.fe.Background"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <!-- HEAD DELLA PAGINA CON LE INCLUSIONI DEI CSS E I META TAG PER LA DESCRIZIONE DEL SITO -->
    <%@include file="head.jsp" %>
    
    <!-- CORPO DELLA PAGINA -->
    <body>
        <!-- NAVBAR A INIZIO PAGINA -->
        <%@include file="header.jsp" %>
        
        <!--
            CONTENUTO EFFETTIVO DELLA PAGINA
            IMPOSTA UNO SFONDO DIVERSO OGNI VOLTA CHE SI RICARICA LA PAGINA
        -->
        <div class="body-container container"
             style="background-image: url('IMAGE/BACKGROUND/<% out.print(Background.getRandomLink()); %>.jpg');">
            
            <!-- LOGO -->
            <div class="text-center">
                <span class="logo">Videoteca</span>
            </div>
            
            <!-- FORM PER LA RICERCA -->
            <form method="post" action="search.jsp" class="form-search text-center" style="padding-right: 40px;">
              <div class="input-append search-bar">
                <input name="q" type="text" class="search-query" style="width: 90%;" autofocus>
                <button type="submit" class="btn">Cerca</button>
              </div>
            </form>
        </div>
             
        <!-- FOOTER PER I CREDITS FISSA A FONDO PAGINA -->
        <%@include file="footer.jsp" %>
    </body>
</html>
