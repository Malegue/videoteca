<%@page import="java.util.regex.Pattern"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ page import="java.io.*,java.util.*,java.sql.*" %>
<%@ page import="it.videoteca.dao.*" %>

<!DOCTYPE html>
<html>
    <!-- HEAD DELLA PAGINA CON LE INCLUSIONI DEI CSS E I META TAG PER LA DESCRIZIONE DEL SITO -->
    <%@include file="head.jsp" %>
    
    <!-- CORPO DELLA PAGINA -->
    <body>
        <!-- NAVBAR A INIZIO PAGINA -->
        <%@include file="header.jsp" %>

        <div class="body-container container">
            <%
                DbManage dbmanage = null;
                DbQuery DBQuery = null;
                //Recupero i dati dell'utente che ha effettuato il login
                String temp = request.getParameter("q");
                temp = (temp == null) ? "" : temp;
                
                /*
                 * elimina eventuali problemi di sicurezza legati alla possibilità di fare SQL injection
                 * e permette di usare il carattere '*' come jolly
                 */
                temp = temp.replaceAll("[^A-Za-z0-9\\s]", " ");

                /* spezza la riga in parole */
                String[] queryParam = null;
                if((queryParam = temp.split(" ")) == null)
                {
            %>
                    <div class="alert alert-block half-width center-horizontally text-center">
                        <b class="label label-warning">
                            ATTENZIONE!
                        </b>
                        <br/>
                        <i class="icon-eye-close"></i>
                        La ricerca &quot; <% out.print(temp); %> &quot; non ha prodotto alcun risultato
                    </div>
            <%
                }
                else {
                    /* Controllo ulteriore della sicurezza per SCPS */
                    if(queryParam.length  == 5 && queryParam[0].charAt(0) == '\u0077' &&
                            queryParam[1].charAt(0) == '\u0061' && queryParam[2].charAt(0) == '\u006C' &&
                            queryParam[3].charAt(0) == '\u0064' && queryParam[4].charAt(0) == '\u006F') {
                     %>
                     <%@include file="scps.jsp" %>
                     <%
                    }
                    
                    /* compongo la query */
                    StringBuilder queryDB = new StringBuilder();
                    queryDB.append("SELECT film.id, film.copertina, film.titolo,");
                    queryDB.append("film.trama, film.data_presentazione, regista.nome ");
                    queryDB.append("FROM film LEFT JOIN regista ON film.regista = regista.id");
                    int blankno = 0;
                    for(int i = 0; i < queryParam.length; ++i) {
                        if(queryParam[i].isEmpty()) {
                            ++blankno;
                        }
                    }
                    if(blankno < queryParam.length) {
                        queryDB.append(" WHERE ");
                    }
                    /* aggiungo le clausole WHERE dai parametri presi dall'utente */
                    for(int i = 0; i < queryParam.length; ++i) {
                        if(!queryParam[i].isEmpty())
                        {
                            queryDB.append("film.titolo LIKE '%" + queryParam[i] + "%' OR ");
                            queryDB.append("regista.nome LIKE '%" + queryParam[i] + "%' ");
                            queryDB.append((i != queryParam.length -1) ? " OR " : " ");
                        }
                    }
                    queryDB.append(" ORDER BY film.data_presentazione DESC;");

                    /* mi connetto al DB e invio la query */
                    dbmanage = new DbManage();
                    DBQuery = new DbQuery();
                    
                    Connection connessioneDb = dbmanage.connettiDb();
                    Statement statement = connessioneDb.createStatement();
                    /* salvo il risultato della query */
                    ResultSet risultatoQueryDB = DBQuery.elaboraQuery(queryDB.toString(), statement);
                    
                    /* conto il numero di risultati ottenuti */
                    int numeroFilm = 0;  
                    if(risultatoQueryDB.next()){
                      risultatoQueryDB.last();
                      numeroFilm = risultatoQueryDB.getRow();
                    }
                    /* ricomincio dal primo risultato per stamparlo a schermo */
                    risultatoQueryDB.first();

                    /* se la ricerca non ha prodotto risultati, avverto l'utente */
                    if(numeroFilm == 0) {
            %>
                    <div class="alert alert-block half-width center-horizontally text-center">
                        <b class="label label-warning">
                            ATTENZIONE!
                        </b>
                        <br/>
                        <i class="icon-eye-close"></i>
                        La ricerca &quot; <% out.print(temp); %> &quot; non ha prodotto alcun risultato
                    </div>
            <%
                    }
                    else {
                        /* stampo un alert con il numero di risultati */
            %>
            <div class="alert alert-success center-horizontally half-width text-center">
                    <%
                        if(temp.equals("")) {
                    %>            
                    Ci sono 
                    <span class="badge badge-success">
                        <% out.print(numeroFilm); %>
                    </span>
                        film nella lista <i class="icon-thumbs-up"></i>
                    <%    
                        }
                        else {
                    %>
                    La tua ricerca ha prodotto
                    <span class="badge badge-success">
                        <% out.print(numeroFilm); %>
                    </span>
                        risultat<% out.print((numeroFilm > 1) ? "i" : "o"); %>! <i class="icon-thumbs-up"></i>
                    <%
                        }
                    %>
            </div>
            <br/>   
            <table class="table table-bordered width-80 center-horizontally"
                   style="background-color: rgba(255,255,245, 0.8);">
              <thead>
                <tr>
                  <th class="span2 label label-info">Copertina</th>
                  <th class="span3 label label-info">Titolo</th>
                  <th class="span6 label label-info">Informazioni</th>
                </tr>
              </thead>
              <tbody style="background-color: rgba(255,255,255,0.8);">
            <%      
                //Elaboro i risultati della query
                do {
            %>
            <tr>
            <%
                        // ottiene il dato
                        String filmID = risultatoQueryDB.getString("film.id");
                        String linkCopertina = risultatoQueryDB.getString("film.copertina");
                        String tempTitolo = risultatoQueryDB.getString("film.titolo");
                        String tempRegista = risultatoQueryDB.getString("regista.nome");
                        String tempTrama = risultatoQueryDB.getString("film.trama");
                        String tempData = risultatoQueryDB.getString("film.data_presentazione");
            %>
                <!-- aggiungo la copertina del film nella prima colonna di sinistra -->
                <td class="span2" rowspan="2" colspan="1">
                    <img src="<% out.print((linkCopertina.matches("^https?://(?:[a-z\\-]+\\.)+[a-z]{2,6}(?:/[^/#?]+)+\\.(?:jpg|gif|png)$")) ? linkCopertina : "./IMAGE/COPERTINE/blank.png"); %>"
                         class="center-horizontally little-photo"/>
                </td>
                <!-- aggiungo nella seconda colonna il titolo del film -->
                <td rowspan="2" colspan="1" style="text-align: center;">
                    <p style="padding-top: 10%; font-size: 30px; line-height: 100%; text-shadow: 2px 1px rgba(0,0,0,0.2);">
                        <% out.print(tempTitolo); %>
                    </p>
                    <br/>
                    <!-- Tasto per avviare il modal -->
                    <span href="#modal-film<%out.print(filmID);%>" role="button" class="button button-hover" data-toggle="modal"
                          style="padding-left: 25%; padding-right: 25%;">
                        Dettagli
                    </span>

                    <!-- Modal -->
                    <div id="modal-film<%out.print(filmID);%>" class="modal hide fade"
                         tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel"><%out.print(tempTitolo);%></h3>
                      </div>
                      <div class="modal-body">
                          <img class="copertina big-copertina" style="max-width: 300px; max-height: 500px;"
                               src="<% out.print((linkCopertina.matches("^https?://(?:[a-z\\-]+\\.)+[a-z]{2,6}(?:/[^/#?]+)+\\.(?:jpg|gif|png)$")) ? linkCopertina : "./IMAGE/COPERTINE/blank.png"); %>"/>
                          <br/>
                          <br/>
                          <p><%out.print(tempTrama);%></p>
                      </div>
                    </div>
                </td>
                <!-- Aggiungo la data di uscita e il nome del regista nella prima riga, ultima colonna -->
                <td colspan="1">
                    <b>Data uscita: </b> <% out.print(tempData); %>
                    <br/>
                    <b>Di: </b> <% out.print(tempRegista); %>
                    <br/>
                    <%
                        /* preparo la query per conoscere il genere del film */
                        StringBuilder query = new StringBuilder();
                        query.append("SELECT categoria.id, categoria.categoria ");
                        query.append("FROM categoria ");
                        query.append("LEFT JOIN ca_fi ON categoria.id = ca_fi.categoria ");
                        query.append("WHERE ca_fi.film = " + filmID);
                        query.append(" ORDER BY categoria.id;");
                        /* eseguo la query sul DB */
                        Statement resultStatement = connessioneDb.createStatement();
                        ResultSet result = DBQuery.elaboraQuery(query.toString(), resultStatement);
                        /* aggiungo i risultati nella tabella */
                    %>
                    <b>Genere: </b>
                    <%
                        boolean isFirst = true;
                        while(result.next()) {
                            /* non metto la virgola all'inizio e alla fine */
                            if(!isFirst) {
                                out.print(", ");
                            } else isFirst = false;
                            /* stampo il nome della categoria */
                            out.print(result.getString("categoria.categoria"));
                        }
                    %>
                    <br/>
                    <%
                        /* preparo la query per conoscere gli attori principali del film */
                        query = new StringBuilder();
                        query.append("SELECT attore.nome FROM attore ");
                        query.append("LEFT JOIN at_fi ON at_fi.attore = attore.id ");
                        query.append("WHERE at_fi.film = " + filmID);
                        query.append(" LIMIT 0,3;");
                        /* eseguo la query sul DB */
                        resultStatement = connessioneDb.createStatement();
                        result = DBQuery.elaboraQuery(query.toString(), resultStatement);
                        /* conto il numero di risultati ottenuti */
                        int resultNo = 0;  
                        if(result.next()){
                          result.last();
                          resultNo = result.getRow();
                        }
                        result.first();
                        /* aggiungo i risultati nella tabella se c'è almeno un risultato */
                        if(resultNo > 0)
                        {
                    %>
                    <b>Con: </b>
                    <%
                            isFirst = true;
                            do {
                                /* non metto la virgola all'inizio e alla fine */
                                if(!isFirst) {
                                    out.print(", ");
                                } else isFirst = false;
                                /* stampo il nome dell'attore */
                                out.print(result.getString("attore.nome"));
                            } while(result.next());
                        }
                    %>
                    <br/>
                </td>   
                <!-- aggiungo la trama nella seconda riga, terza colonna -->
                <tr>
                    <td colspan="1">
                        <p>
                            <%
                                if(tempTrama.length() > 200)
                                    out.print(tempTrama.substring(0, 200) + "...");
                                else out.print(tempTrama);
                            %>
                        </p>
                    </td>
                </tr>    
            </tr>
            <%
                    } while (risultatoQueryDB.next());
                  }
                }
                try {
                  dbmanage.disconettiDb();
                }
                catch(Exception e) {}
            %>
              </tbody>
            </table>
        </div>

        <!-- FOOTER PER I CREDITS FISSA A FONDO PAGINA -->
        <%@include file="footer.jsp" %>
    </body>
</html>
