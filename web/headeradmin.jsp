<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- NAVBAR A INIZIO DI TUTTE LE PAGINE -->
<header>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <!-- TITOLO A INIZIO DELLA NAVBAR -->
            <a class="brand">Gestione</a>
            <!-- IL RESTO DELLA NAVBAR DOPO IL TITOLO -->
            <div class="nav-collapse collapse">
                <!-- MENU' A SINISTRA CON I LINK ALLE SEZIONI DEL SITO -->
                <%
                    String parameter = request.getParameter("action");
                    parameter = (parameter == null) ? "" : parameter;
                %>
                <ul class="nav">
                    <li class="<% out.print(parameter.isEmpty() ? "active" : "navbar-focus"); %>">
                        <a href="manage.jsp"><i class="icon-home"></i>  Home</a>
                    </li>
                    <li class="<% out.print(parameter.equals("insert") ? "active" : "navbar-focus"); %>">
                        <a href="manage.jsp?action=insert"><i class="icon-film"></i>  Inserisci</a>
                    </li>
                    <li class="<% out.print(parameter.equals("delete") ? "active" : "navbar-focus"); %>">
                        <a href="manage.jsp?action=delete"><i class="icon-film"></i>  Elimina</a>
                    </li>
                </ul>
                <!-- MENU' A DESTRA CON IL LINK ALLA SEZIONE ABOUT DEL SITO -->
                <ul class="nav pull-right">
                    <li class="navbar-focus">
                        <a href="admin?action=logout"><i class="icon-off"></i> Log Out</a>
                    </li>
                </ul>
            </div>
        </div>
      </div>
    </div>
</header>